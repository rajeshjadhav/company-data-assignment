const initialState = {
    companiesListData: [],
    name: '',
    description: '',
    editId: 0,
};

const companies = (state = initialState, action) => {
    let newState;
    switch (action.type) {
        case 'GET_ALL_COMAPNIES':
            newState = { ...state }
            newState.companiesListData.push(...action.data);
            return newState;
        case 'ADD_COMPANY':
            newState = { ...state };
            newState.companiesListData.push(action.data.newCompany);
            return newState;
        case 'EDIT_COMPANY':
            newState = { ...state };
            newState.editId = action.data.id;
            return newState;
        case 'UPDATE_COMPANY':
            newState = { ...state };
            newState.companiesListData.forEach(element => {
                if (newState.editId === element.id) {
                    element.name = action.data.name;
                    element.description = action.data.description;
                }
            });
            return newState;
        case 'DELETE_COMPANY':
            newState = { ...state };
            let afterDeleteState = newState.companiesListData.filter(item => {
                return item.id !== action.data.id
            });
            newState.companiesListData = afterDeleteState;
            return newState;
        default:
            return state;
    }
}

export default companies;