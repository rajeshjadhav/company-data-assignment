export const addCompany = (data) => {
    return dispatch => {
        dispatch({
            type: 'ADD_COMPANY',
            data: { newCompany: data }
        });
    }
}
export const editCompany = (data) => {
    return dispatch => {
        dispatch({
            type: 'EDIT_COMPANY',
            data
        });
    }
}
export const updateCompany = (data) => {
    return dispatch => {
        dispatch({
            type: 'UPDATE_COMPANY',
            data
        });
    }
}
export const deleteCompany = (data) => {
    return dispatch => {
        dispatch({
            type: 'DELETE_COMPANY',
            data
        });
    }
}
export const getAllCompanies = () => {
    return dispatch => {
        const initialState = [
            {
                id: 1,
                name: 'Cybage',
                description: 'Cybage Software is an information technology consulting company founded in 1995 in Kalyani Nagar, Pune, India. The company provides software application development and maintenance, with development centers in Pune, Hyderabad and Gandhinagar in India.'
            },
            {
                id: 2,
                name: 'Capgemini',
                description: 'Capgemini SE is a French multinational professional services and business consulting corporation headquartered in Paris, France.'
            },
            {
                id: 3,
                name: 'Infosys',
                description: 'Infosys Limited is an Indian multinational corporation that provides business consulting, information technology and outsourcing services. It has its headquarters in Bengaluru, Karnataka, India.'
            }
        ];
        dispatch({
            type: 'GET_ALL_COMAPNIES',
            data: initialState
        });
    }
}
