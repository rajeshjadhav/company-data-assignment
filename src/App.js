import React from 'react';
import { Provider } from "react-redux";
import store from "./store/";
import CompanyForm from './containers/CompanyForm/';
import CompaniesListView from './containers/CompaniesListView/';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <React.Fragment>
          <Provider store={store}>
            <React.Fragment>
              <CompanyForm />
              <CompaniesListView />
            </React.Fragment>
          </Provider>
        </React.Fragment>
      </div>
    );
  }
}

export default App;