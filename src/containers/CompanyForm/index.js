import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import CompanyFormComponent from '../../components/CompanyForm/';
import * as companyActionCreator from '../../actions/companyAction';

class CompanyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.companies.name,
            description: this.props.companies.description,
        };
    }
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    }
    onSubmit = event => {
        event.preventDefault();
        const { name, description } = this.state;
        const id = new Date().getTime();
        const { companyAction, companies } = this.props;
        if (companies.editId === 0) {
            companyAction.addCompany({ id, name, description });
        }
        else {
            companyAction.updateCompany({ name, description });
        }
        this.setState({
            name: '',
            description: ''
        });
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.companies.editId !== nextProps.companies.editId && nextProps.companies.editId !== 0) {
            const { companies } = nextProps;

            const editData = companies.companiesListData.find(item => {
                return item.id === companies.editId
            });
            this.setState({
                name: editData.name,
                description: editData.description
            });
        }
    }
    render() {
        const { editId } = this.props.companies;
        return (
            <div className="App">
                <CompanyFormComponent
                    stateData={this.state}
                    handleChange={this.handleChange}
                    onSubmit={this.onSubmit}
                    editId={editId}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        companies: state.companiesData
    }
}

const mapDispatchToProps = (dispatch) => ({
    companyAction: bindActionCreators(companyActionCreator, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(CompanyForm);