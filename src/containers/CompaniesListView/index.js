import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import CompaniesListViewComponent from '../../components/CompaniesListView/';
import * as companyActionCreator from '../../actions/companyAction.js';

class CompaniesListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editFlag: false,
            editId: 0
        }
    }

    componentWillMount() {
        const { companyAction } = this.props;
        companyAction.getAllCompanies();
    }
    onEdit = id => event => {
        const { companyAction } = this.props;
        companyAction.editCompany({ id });
    }
    onDelete = id => event => {
        const { companyAction } = this.props;
        companyAction.deleteCompany({ id });
    }
    render() {
        const { companiesListData, editId } = this.props.companies;
        return (
            <div className="App">
                {(companiesListData && companiesListData.length > 0) ? <CompaniesListViewComponent
                    companiesListData={companiesListData}
                    onEdit={this.onEdit}
                    onDelete={this.onDelete}
                    editId={editId} />
                    : ''}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        companies: state.companiesData
    }
}

const mapDispatchToProps = (dispatch) => ({
    companyAction: bindActionCreators(companyActionCreator, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(CompaniesListView);