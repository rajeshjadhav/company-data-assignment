import { applyMiddleware, createStore, combineReducers } from "redux";
import thunk from "redux-thunk";
import ComapniesReducer from "../reducers/Companies.js";

const middleware = applyMiddleware(thunk);

const store = createStore(
    combineReducers({
        companiesData: ComapniesReducer
    })
    , middleware
);

export default store;