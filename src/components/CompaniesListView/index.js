import React from "react";
import { Card, Grid, CardActions } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
    container: {
        padding: 10
    },
    card: {
        minWidth: 275,
        maxWidth: 275,
        margin: 5
    },
    title: {
        marginBottom: 16,
        fontSize: 14,
    },
};

const CompaniesListView = ({ companiesListData, onEdit, onDelete, classes }) => {
    const companiesListElement = companiesListData.map((company, i) =>
        <Card className={classes.card} key={i}>
            <CardContent>
                <Typography variant="headline" component="h2">
                    {company.name}
                </Typography>
                <Typography component="p">
                    {company.description}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" variant="contained" color="primary"
                    onClick={onEdit(company.id)}>Edit </Button>
                <Button size="small" variant="contained" color="default"
                    onClick={onDelete(company.id)}>Delete </Button>
            </CardActions>
        </Card>
    );
    return (
        <Grid container item justify='center' className={classes.container}>
            <Grid container item xs={8} >
                {companiesListElement}
            </Grid>
        </Grid>
    );
}

export default withStyles(styles)(CompaniesListView);