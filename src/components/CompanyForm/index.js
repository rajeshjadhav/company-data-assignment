import React from "react";
import { Button, Paper, Grid, TextField } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles/';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    paper: {
        padding: 10,
        margin: 10,
        width: '100%',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 500,
    }
});
const CompanyForm = ({ classes, stateData, handleChange, onSubmit, editId }) => {
    const { name, description } = stateData;
    return (
        <Grid container item justify='center' className={classes.border}>
            <Grid container item xs={8} className={classes.border}>
                <Paper className={classes.paper}>
                    <form className={classes.container}
                        onSubmit={onSubmit}>
                        <TextField
                            label="Company Name"
                            className={classes.textField}
                            value={name}
                            onChange={handleChange('name')}
                        />
                        <TextField
                            label="Company Description"
                            multiline
                            rowsMax="4"
                            value={description}
                            onChange={handleChange('description')}
                            className={classes.textField}
                        />
                        <Button type="submit" variant="contained" color="primary">
                            Submit
                        </Button>
                    </form>
                </Paper>
            </Grid>
        </Grid>
    );
}
export default withStyles(styles)(CompanyForm);